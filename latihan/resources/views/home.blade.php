<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Home</title>
</head>
<body>
    <h1>Media Online</h1>
    <h2>Social Media Developer</h2>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    <p><b>Manfaat Gabung di Media Online</b></p>
    <ul>
        <li>Mendapatkan motivasi dari sesama developer</li>
        <li>Berbagi pengetahuan</li>
        <li>Dibuat oleh calon <i>web developer</i> terbaik</li>
    </ul>
    <p><b>Cara Bergabung ke Media Online</b></p>
    <ol>
        <li>Mengunjungi situs web ini</li>
        <li>Mendaftar di <a href="/register">Sign Up Form</a></li>
        <li>Selesai</li>
    </ol>
</body>
</html>
